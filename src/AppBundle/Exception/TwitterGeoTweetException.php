<?php

namespace AppBundle\Exception;

/**
 * 
 * any exception related to TwitterGeoTweet
 * @author andrew
 */
class TwitterGeoTweetException extends TwitterException
{
    
}
