<?php

namespace AppBundle\Exception;

/**
 * CacheManager operation Exception
 *
 * @author andrew
 */
class CacheException extends \Exception
{

}
