<?php

namespace AppBundle\Service\TwitterGeoSearch;

use AppBundle\Service\TwitterApi\TwitterApiInterface;
use AppBundle\Service\CacheManager\CacheManagerInterface;

/**
 *
 * @author andrew
 */
interface TwitterGeoSearchInterface
{

    /**
     * 
     * @param \AppBundle\Service\TwitterGeoSearch\TwitterApiInterface $twitterApiWrapper
     * @param \AppBundle\Service\CacheManager\CacheManagerInterface $cacheManager
     */
    public function __construct(TwitterApiInterface $twitterApiWrapper, CacheManagerInterface $cacheManager);

    /**
     * search twitter for tweets with geo data
     * @param string $query
     * @return AppBundle\Service\TwitterGeoSearch\TwitterGeoResponse
     * @throws AppBundle\Exception\TwitterGeoSearchException
     */
    public function search($query);
}
