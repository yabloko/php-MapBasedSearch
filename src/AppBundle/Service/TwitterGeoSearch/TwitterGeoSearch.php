<?php

namespace AppBundle\Service\TwitterGeoSearch;

use AppBundle\Service\TwitterApi\TwitterApiInterface;
use AppBundle\Service\CacheManager\CacheManagerInterface;
use AppBundle\Exception\TwitterException;
use AppBundle\Exception\TwitterGeoSearchException;
use AppBundle\Exception\CacheException;
use AppBundle\Document\TwitterGeoResponse;

/**
 * Description of TwitterGeoSearch
 *
 * @author andrew
 */
class TwitterGeoSearch implements TwitterGeoSearchInterface
{

    /**
     * 
     * @var AppBundle\Service\TwitterApi\TwitterApiInterface
     */
    protected $twitterApi;

    /**
     *
     * @var CacheManagerInterface
     */
    protected $cacheManager;

    /**
     * id of a user session
     * @var string
     */
    protected $sessionId;

    public function __construct(TwitterApiInterface $twitterApiWrapper, CacheManagerInterface $cacheManager)
    {
        $this->twitterApi = $twitterApiWrapper;
        $this->cacheManager = $cacheManager;
    }

    public function search($query)
    {
        $formattedQuery = $this->validateAndFormatQuery($query);
        try {
            $queryObject = new TwitterGeoResponse($formattedQuery);
            $result = $this->cacheManager->get($queryObject);
            if (!$result) {
                $result = $this->twitterApi->search($formattedQuery);
                $this->cacheManager->put($result);
            }
            return $result;
        } catch (TwitterException $e) {
            throw $e;
        } catch (CacheException $e) {
            throw new TwitterException(null, null, $e);
        }
    }

    /**
     * validates input query and formats it
     * @param string $query
     * @return string
     * @throws AppBundle\Exception\TwitterGeoSearchException
     */
    protected function validateAndFormatQuery($query)
    {
        $formattedQuery = preg_replace('[^a-zA-Z0-9\s]', '', $query);
        if (strlen($formattedQuery) > 50) {
            throw new TwitterGeoSearchException('query is too long');
        }
        return $formattedQuery;
    }

}
