<?php

namespace AppBundle\Service\CacheManager;

/**
 * cachable entity must have 
 * @property \DateTime $dateCreated to define if it should be stored in cache or dropped
 * @property string $sessionId to link it to particular session in storage
 * @author andrew
 */
interface CachableDocument
{

    /**
     * @return \DateTime
     */
    public function getDateCreated();

    /**
     * @param \DateTime $date
     */
    public function setDateCreated($date);

    /**
     * @return string http session unique id
     */
    public function getSessionId();

    /**
     * @param string $id
     * @throws AppBundle\Exception\TwitterGeoSearchException if sessionId is invalid
     */
    public function setSessionId($id);
}
