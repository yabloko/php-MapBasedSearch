<?php

namespace AppBundle\Service\CacheManager;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * CacheManager implementation should store documents in db for
 * $timeout time, and remove based on dateCreated attribute
 * item can be selected from cache based on query, sessionId is managed automatically
 * @author andrew
 */
interface CacheManagerInterface
{

    /**
     * 
     * @param \AppBundle\Service\TwitterGeoSearch\ObjectManager $documentManager
     * @param \AppBundle\Service\TwitterGeoSearch\Session $session
     * @param int $timeout time to store data in cache
     */
    public function __construct(ObjectManager $documentManager, Session $session, $timeout);

    /**
     * put twitter lookup results to storage
     * @param AppBundle\CacheManager\CachableDocument $result
     * @throws \AppBundle\Exception\CacheException
     */
    public function put(CachableDocument $result);

    /**
     * get cached results by query
     * @param AppBundle\Service\CacheManager\CachableDocument $query object,
     * non-null gettable fields will be used as query parameters
     * @return AppBundle\Document\TwitterGeoResponse
     * @throws \AppBundle\Exception\CacheException
     */
    public function get(CachableDocument $query);
}
