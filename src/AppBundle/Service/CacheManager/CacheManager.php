<?php

namespace AppBundle\Service\CacheManager;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Document\TwitterGeoResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Exception\CacheException;

/**
 * @author andrew
 */
class CacheManager implements CacheManagerInterface
{

    /**
     * object to manipulate with database,
     * mongo DocumentManager
     * @var type 
     */
    protected $documentManager;

    /**
     *
     * @var int cache timeout in seconds 
     */
    protected $timeout;

    /**
     * id of a user's session
     * @var string
     */
    protected $sessionId;

    public function __construct(ObjectManager $documentManager, Session $session, $timeout)
    {
        $this->sessionId = $session->getId();
        $this->documentManager = $documentManager;
        $this->timeout = $timeout;
    }

    /**
     * remove outdated records for all sessions
     * from cache
     * @param \AppBundle\Service\CacheManager\CachableDocument $queryObject
     * @throws \Exception
     */
    protected function clearCache(CachableDocument $queryObject)
    {
        $this->timeout;
        $reflection = new \ReflectionClass($queryObject);
        $repositoryPath = 'AppBundle:' . $reflection->getShortName();
        $mongoDate = new \MongoDate(time() - $this->timeout);
        $q = $this->documentManager->createQueryBuilder($repositoryPath)
                ->remove()
                ->field('dateCreated')->lt($mongoDate)
                ->getQuery();
        $q->execute();
        $this->documentManager->flush();
    }

    public function get(CachableDocument $queryObject)
    {
        try {
            $this->clearCache($queryObject);
            $reflection = new \ReflectionClass($queryObject);
            $repositoryPath = 'AppBundle:' . $reflection->getShortName();
            $queryObject->setSessionId($this->sessionId);
            $query = array();
            $getters = array_filter($reflection->getMethods(), function($method) {
                return strpos($method->name, 'get') === 0;
            });
            foreach ($getters as $getter) {
                $attribute = lcfirst(substr($getter->name, 3));
                $value = $queryObject->{$getter->name}();
                if ($value && $value !== null) {
                    $query[$attribute] = $value;
                }
            }
            $cachedResponse = $this->documentManager
                    ->getRepository($repositoryPath)
                    ->findOneBy($query);
            return $cachedResponse;
        } catch (\Exception $e) {
            throw new CacheException(null, null, $e);
        }
    }

    public function put(CachableDocument $result)
    {
        try {
            $this->clearCache($result);
            $result->setSessionId($this->sessionId);
            $result->setDateCreated(new \DateTime());
            $this->documentManager->persist($result);
            $this->documentManager->flush();
        } catch (\Exception $e) {
            throw new CacheException(null, null, $e);
        }
    }

}
