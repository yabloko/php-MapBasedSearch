<?php

namespace AppBundle\Service\TwitterApi;

use AppBundle\Document\TwitterGeoTweet;
use AppBundle\Document\TwitterGeoResponse;
use AppBundle\Exception\TwitterApiException;

/**
 * wrapper for any third party twitter API library
 * to keep unified interface in this app
 * please implement other versions with underlying lib lazy init
 * on actual search() call if possible
 *
 * @author andrew
 */
class TwitterApiWrapper implements TwitterApiInterface
{

    /**
     * external library to access twitter
     * @var array $tokens creds to access twitter API
     * @see \TwitterAPIExchange for details
     */
    protected $twitterApi;

    public function __construct(\TwitterAPIExchange $twitterApiExchange)
    {
        $this->twitterApi = $twitterApiExchange;
    }

    public function search($query)
    {
        //if this method was requested, actually init twitter and query it
        //look for palces and find first one. find center of a bounding box and
        //request tweets by coordinates and radius
        $apiResponseJson = $this->twitterApi->performRequest();
        if (!$apiResponseJson) {
            throw new TwitterApiException('could not get response from twitter api');
        }
        $apiResponse = json_decode($apiResponseJson, true);
        $this->validateApiResponse($apiResponse);
        $results = array();
        $lng = 150.00;
        $lat = -34.00;
        foreach ($apiResponse['statuses'] as $apiTweet) {
            if (!array_key_exists('coordinates', $apiTweet)) {
                continue;
            }
            $rLng1 = $apiTweet['coordinates']['coordinates'][0];
            $rLat1 = $apiTweet['coordinates']['coordinates'][1];
            $hashTags = array_map(
                    function($value) {
                return $value['text'];
            }, $apiTweet['entities']['hashtags']);
            $results[] = new TwitterGeoTweet($rLng1, $rLat1, null, $hashTags);
        }
        return new TwitterGeoResponse($query, $lng, $lat, $results);
    }

    protected function validateApiResponse($response)
    {
        if (!array_key_exists('statuses', $response)) {
            throw new TwitterApiException('response is empty - missing root \'statuses\' node');
        }
        foreach($response['statuses'] as $t){
            if(!is_array($t) || !array_key_exists('entities', $t) || !array_key_exists('hashtags', $t['entities'])){
                throw new TwitterApiException('response is broken - \'entities\' or \'hashtags\' missing');
            }
        }
    }

}
