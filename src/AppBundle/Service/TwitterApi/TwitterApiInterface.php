<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Service\TwitterApi;

/**
 *
 * @author andrew
 */
interface TwitterApiInterface
{
    /**
     * request result directly from twitter
     * @param string $query
     * @return AppBundle\Document\TwitterGeoResponse
     * @throws AppBundle\Exception\TwitterApiException
     */
    public function search($query);
}
