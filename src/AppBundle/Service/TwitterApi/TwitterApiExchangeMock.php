<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Service\TwitterApi;

/**
 * Description of TwitterApiExchangeMock
 *
 * @author andrew
 */
class TwitterApiExchangeMock extends \TwitterAPIExchange
{
    public function __construct($tokens)
    {
        
    }
    
    public function performRequest($return = true, $curlOptions = array())
    {
        return file_get_contents('/opt/myTrainingRepos/myPhp/MapBasedSearch/src/AppBundle/Tests/Service/TwitterApi/twitterApiResponse.json');
    }
}
