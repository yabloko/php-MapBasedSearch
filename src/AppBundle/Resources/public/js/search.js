var map;

/**
 * refresh page and append search query, so that each search has it's url
 * and can be copypasted to share
 * @param {string} inputElementId
 * @returns {undefined}
 */
function search(inputElementId) {
    searchQuery = document.getElementById(inputElementId).value;
    window.location.href = '/index?q=' + searchQuery;
}

/**
 * callback to google maps API,
 * queries server for search results
 * @returns {undefined}
 */
function onLoad() {
    var query = getURLParameter('q');
    if (query) {
        $.get('search?query=' + query, showSearchResults);
    }

}

/**
 * http get callback, passes response data to initMap
 * @param {array} data
 * @param {type} status
 * @returns {undefined}
 */
function showSearchResults(data, status) {
    if (!status) {
        throw Error('status is' + status);
    }
    coordinates = jQuery.parseJSON(data);
    initMap(jQuery.parseJSON(data));
}

/**
 * copypasted from internets, function allows to get GET parameter 
 * from URL by name
 * @param {string} sParam
 * @returns {undefined}
 */
function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam)
        {
            return sParameterName[1];
        }
    }
}

/**
 * init google map and places all markers listed in coordinates array
 * @param {array} coordinates
 * @returns {undefined}
 */
function initMap(coordinates) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: coordinates.latitude, lng: coordinates.longtitude},
        zoom: 8
    });
    coordinates.tweets.forEach(function (item) {
        var marker = new google.maps.Marker({
            position: {lat: item.latitude, lng: item.longtitude},
            map: map,
            title: item.hashTags[0]
        });
    });

}

if (getURLParameter('q')) {
    document.getElementById('searchInput').value = getURLParameter('q');
}