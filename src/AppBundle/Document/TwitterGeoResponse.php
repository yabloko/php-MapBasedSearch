<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use AppBundle\Document\TwitterGeoTweet;
use AppBundle\Service\CacheManager\CachableDocument;
use AppBundle\Exception\TwitterGeoSearchException;

/**
 * results from twitter
 *
 * @author andrew
 * @MongoDB\Document
 */
class TwitterGeoResponse implements \JsonSerializable, CachableDocument
{

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $sessionId;

    /**
     * json encoded results
     * @MongoDB\String
     */
    protected $jsonEncodedResults;

    /**
     * initial query that gave this response
     * @MongoDB\String
     */
    protected $query;

    /**
     * 
     * @MongoDB\Date
     */
    protected $dateCreated;

    /**
     * 
     * @MongoDB\Float
     */
    protected $longtitude;

    /**
     * 
     * @MongoDB\Float
     */
    protected $latitude;

    public function __construct($query = null, $lng = null, $lat = null, $tweets = null)
    {
        $this->query = $query;
        $this->longtitude = $lng;
        $this->latitude = $lat;
        $this->setTweets($tweets);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getJsonEncodedResults()
    {
        return $this->jsonEncodedResults;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setSessionId($sessionId)
    {
        if (!$sessionId) {
            throw new TwitterGeoSearchException(sprintf('sessionId cannot be blank'));
        }
        $this->sessionId = $sessionId;
    }

    public function setJsonEncodedResults($jsonEncodedResults)
    {
        $this->jsonEncodedResults = $jsonEncodedResults;
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLongtitude($longtitude)
    {
        $this->longtitude = $longtitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * 
     * @return AppBundle\Document\TwitterGeoTweet
     */
    public function getTweets()
    {
        $arrayData = json_decode($this->jsonEncodedResults, true);
        $results = array();
        if ($arrayData) {
            foreach ($arrayData as $json) {
                $results[] = TwitterGeoTweet::jsonDeserialize(json_encode($json));
            }
        }
        return $results;
    }

    /**
     * 
     * @param AppBundle\Document\TwitterGeoTweet[] $geoResponses
     */
    public function setTweets($geoResponses)
    {
        if ($geoResponses) {
            $this->jsonEncodedResults = json_encode($geoResponses);
        }
    }

    public function jsonSerialize()
    {
        $result = array();
        $result['query'] = $this->query;
        $result['longtitude'] = $this->longtitude;
        $result['latitude'] = $this->latitude;
        $result['tweets'] = $this->getTweets();
        return $result;
    }

}
