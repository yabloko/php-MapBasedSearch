<?php

namespace AppBundle\Document;

use AppBundle\Exception\TwitterGeoTweetException;

/**
 * container for twitter search response data
 * 
 * @author andrew
 */
class TwitterGeoTweet implements \JsonSerializable
{

    /**
     *
     * @var double
     */
    private $longtitude;

    /**
     *
     * @var double
     */
    private $latitude;

    /**
     *
     * @var string
     */
    private $pictureUrl;

    /**
     *
     * @var string[]
     */
    private $hashTags;

    public function __construct($longtitude, $latitude, $pictureUrl, $hashTags)
    {
        $this->longtitude = $longtitude;
        $this->latitude = $latitude;
        $this->pictureUrl = $pictureUrl;
        $this->hashTags = $hashTags;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    public function getHashTags()
    {
        return $this->hashTags;
    }

    public function setLongtitude($longtitude)
    {
        $this->longtitude = $longtitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;
    }

    public function setHashTags($hashTags)
    {
        $this->hashTags = $hashTags;
    }

    public function jsonSerialize()
    {
        return array('latitude' => $this->getLatitude(),
            'longtitude' => $this->getLongtitude(),
            'hashTags' => $this->getHashTags(),
            'pictureUrl' => $this->getPictureUrl());
    }

    /**
     * decodes array
     * @param string $json json-encoded AppBundle\Document\TwitterGeoTweet object 
     * @see TwitterGeoTweet::jsonSerialize for format
     * @return \AppBundle\Document\TwitterGeoTweet
     * @throws \AppBundle\Exception\TwitterGeoTweetException
     */
    public static function jsonDeserialize($json)
    {
        $arrayData = json_decode($json, true);
        try {
            return new TwitterGeoTweet($arrayData['longtitude'], $arrayData['latitude'], $arrayData['pictureUrl'], $arrayData['hashTags']);
        } catch (\Exception $e) {
            throw new TwitterGeoTweetException(sprintf('mapping json to [%s] error', get_class()), 0, $e);
        }
    }

}
