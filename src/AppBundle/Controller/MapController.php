<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Session\Session;

class MapController extends Controller
{

    /**
     * @Route("/index")
     */
    public function indexAction(Request $request)
    {

        $session = $request->getSession();
        if (!$session->isStarted()) {
            $session->start();
        }

        $key = '%api_key%';
        return $this->render('AppBundle:MapController:index.html.twig', array('googleApiKey' => $key));
    }

    /**
     * @Route("/search")
     */
    public function searchAction(Request $request)
    {
        $query = $request->query->get('query');
        $searchService = $this->get('twitter_geo_search');
        try {
            $twitterResponse = $searchService->search($query);
            return new Response(json_encode($twitterResponse), 200, array('Content-type: app/json'));
        } catch (\Exception $e) {
            /*
             * @todo log exception and show HTTP 500
             */
            throw $e;
        }
    }

}
