<?php

namespace AppBundle\Service\CacheManager;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-11-13 at 10:27:32.
 */
class CacheManagerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var CacheManager
     */
    protected $object;

    /**
     * CachableDocument implementation
     * @var AppBundle\Service\CacheManager\CachableDocument
     */
    protected $cachableDocument;
    protected $repository;
    protected $documentManager;
    protected $sessionId = 'sessionId';

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->cachableDocument = $this->getMock('AppBundle\Service\CacheManager\CachableDocument');
        $session = $this->getMockBuilder('Symfony\Component\HttpFoundation\Session\Session')
                ->disableOriginalConstructor()
                ->getMock();
        $session->method('getId')->willReturn($this->sessionId);
        $this->documentManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager', array('createQueryBuilder', 'find', 'persist', 'remove', 'merge', 'clear', 'detach', 'refresh', 'flush', 'getRepository', 'getClassMetadata', 'getMetadataFactory', 'initializeObject', 'contains'));

        $queryBuilder = $this->getMock('Query', array('execute', 'getQuery', 'lt', 'field', 'remove'));
        $queryBuilder->method('execute')->willReturn(true);
        $queryBuilder->method('getQuery')->willReturn($queryBuilder);
        $queryBuilder->method('lt')->willReturn($queryBuilder);
        $queryBuilder->method('field')->willReturn($queryBuilder);
        $queryBuilder->method('remove')->willReturn($queryBuilder);

        $this->documentManager->method('createQueryBuilder')->willReturn($queryBuilder);
        $this->documentManager->method('flush')->willReturn(true);
        $this->documentManager->method('persist')->willReturn(true);
        $this->repository = $this->getMock('Repository', array('findOneBy'));
        $this->documentManager->method('getRepository')->willReturn($this->repository);
        $this->object = new CacheManager($this->documentManager, $session, 1);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers AppBundle\Service\CacheManager\CacheManager::get
     */
    public function testGet()
    {
        //not sure if we need this
        //$this->documentManager->expects($this->once())->method('flush')->willReturn(true);
        $this->repository->method('findOneBy')->willReturn($this->cachableDocument);
        $queryDocument = $this->getMock('AppBundle\Service\CacheManager\CachableDocument');
        self::assertEquals($this->cachableDocument, $this->object->get($queryDocument));
    }

    /**
     * @covers AppBundle\Service\CacheManager\CacheManager::get
     */
    public function testDocumentNotExistGet()
    {
        $this->documentManager->expects($this->once())->method('flush');
        $this->repository->method('findOneBy')->willReturn(null);
        $queryDocument = $this->getMock('AppBundle\Service\CacheManager\CachableDocument');
        self::assertEquals(null, $this->object->get($queryDocument));
    }

    /**
     * @covers AppBundle\Service\CacheManager\CacheManager::put
     */
    public function testPut()
    {
        $this->documentManager->expects($this->once())->method('persist');
        $this->documentManager->expects($this->exactly(2))->method('flush');
        $this->cachableDocument->expects($this->once())->method('setDateCreated');
        $this->cachableDocument->expects($this->once())->method('setSessionId')->with($this->sessionId);
        $this->object->put($this->cachableDocument);
    }

    /**
     * @covers AppBundle\Service\CacheManager\CacheManager::get
     * @expectedException AppBundle\Exception\CacheException
     */
    public function testGetCacheException()
    {
        $this->repository->method('findOneBy')->will($this->throwException(new \Exception));
        $queryDocument = $this->getMock('AppBundle\Service\CacheManager\CachableDocument');
        $this->object->get($queryDocument);
    }

    /**
     * @covers AppBundle\Service\CacheManager\CacheManager::put
     * @expectedException AppBundle\Exception\CacheException
     */
    public function testPutCacheException()
    {
        $this->documentManager->method('flush')->will($this->throwException(new \Exception));
        $this->object->put($this->cachableDocument);
    }
}
